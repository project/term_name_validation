<?php

namespace Drupal\term_name_validation\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks that the node.
 *
 * @Constraint(
 *   id = "TermNameValidate",
 *   label = @Translation("Term Name Validation"),
 * )
 */
class TermNameConstraint extends Constraint {

}
