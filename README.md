# Term Name Validation

This is a lightweight module that helps to restrict the term names with
blacklist characters/words, character length, word count and unique name.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/term_name_validation).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/term_name_validation).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. 
For further information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Configure user permissions in Administration » People » Permissions:
   Term Name validation admin control

2. Users in roles with the "Delete Social Post user accounts" permission
   can perform administration tasks for Term Name validation module.

3. Configure Term Name Validation in Modules » Configure OR
   goto Configuration » User interface » Term name validation
   (/admin/config/term_name_validation/term-name-validation)
